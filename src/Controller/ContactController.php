<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Contact;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(): Response
    {

        $contacts = $this->getDoctrine()->getRepository(Contact::class)->findAll();

        return $this->render('contact/index.html.twig', [
            'contacts' => $contacts,
        ]);
    }

    /**
     * @Route("/contact/new", name="new")
     */
    public function new(){

        return $this->render('contact/new.html.twig');

    }

    /**
     * @Route("/contact/edit/{id}", name="edit")
     */
    public function edit(Contact $id){

        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($id);

        return $this->render('contact/edit.html.twig', ['contact'=>$contact]);

    }

    /**
     * @Route("/contact/show/{id}", name="show")
     */
    public function show(Contact $id){

        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($id);

        return $this->render('contact/show.html.twig', ['contact'=>$contact]);

    }


    /**
     * @Route("/contact/new/create", name="create")
     */
    public function create(Request $request){

        $name = $request->request->get('name');
        $surname = $request->request->get('surname');
        $email = $request->request->get('email');
        $cel = $request->request->get('cel');

        $contact = (new Contact())
            ->setName($name)
            ->setSurname($surname)
            ->setEmail($email)
            ->setCel($cel);

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($contact);
        $entityManager->flush();

        return $this->redirectToRoute('contact');

    }

    /**
     * @Route("/contact/edit/{id}/update", name="update")
     */
    public function update(Contact $id, Request $request){

        $name = $request->request->get('name');
        $surname = $request->request->get('surname');
        $email = $request->request->get('email');
        $cel = $request->request->get('cel');

        $entityManager = $this->getDoctrine()->getManager();

        $contact = $entityManager->getRepository(Contact::class)->find($id);

        $contact->setName($name);
        $contact->setSurname($surname);
        $contact->setEmail($email);
        $contact->setCel($cel);

        $entityManager->flush();

        return $this->redirectToRoute('contact');

    }

    /**
     * @Route("/contact/delete/{id}", name="delete")
     */
    public function delete(Contact $id){

        $entityManager = $this->getDoctrine()->getManager();
        
        $entityManager->remove($id);
        $entityManager->flush();
        

        return $this->redirectToRoute('contact');

    }
}
